@extends('layout/main')

@section('title', 'Daftar Pelanggan')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-10">  
                <h1 class="mt-5">Daftar Pelanggan</h1>
                <a href="/pelanggan/create" class="btn btn-success plus float-right my-3">Tambah Pelanggan!</a>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Kode Pelanggan</th>
                            <th scope="col">Nama Pelanggan</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Nama Kota</th>
                            <th scope="col">No Telepon</th> 
                            <th scope="col">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $pelanggan as $plgn)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $plgn->kode_pelanggan }}</td>
                            <td>{{ $plgn->nama_pelanggan }}</td>
                            <td>{{ $plgn->alamat }}</td>
                            <td>{{ $plgn->nama_kota }}</td>
                            <td>{{ $plgn->no_telepon }}</td>
                            <td>
                                <a href="" class="btn btn-success">Edit</a>
                                <form method="POST" action="{{ $plgn-> id }}" class="d-inline">
                                    <input type="hidden" name="_method" value="delete" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection
