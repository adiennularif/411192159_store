@extends('layout/main')

@section('title', 'Form Tambah Pelanggan')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-10">  
                <h1 class="mt-3">Form Tambah Pelanggan</h1>
                <form method="POST" action="/pelanggan" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="kode_pelanggan" class="form-label">Kode Pelanggan</label>
                        <input type="number" class="form-control" id="kode_pelanggan" placeholder="Masukan Kode Pelanggan" name="kode_pelanggan">
                    </div>
                    <div class="form-group">
                        <label for="nama_pelanggan" class="form-label">Nama Pelanggan</label>
                        <input type="text" class="form-control" id="nama_pelanggan" placeholder="Masukan Nama Pelanggan" name="nama_pelanggan">
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="form-label">Alamat</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Masukan alamat" name="alamat">
                    </div>
                    <div class="form-group">
                        <label for="nama_kota" class="form-label">Nama Kota</label>
                        <input type="text" class="form-control" id="nama_kota" placeholder="Masukan Nama Kota" name="nama_kota">
                    </div>
                    <div class="form-group">
                        <label for="no_telepon" class="form-label">No Telepon</label>
                        <input type="number" class="form-control" id="no_telepon" placeholder="Masukan No Telepon" name="no_telepon">
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah Data!</button>
                </form>
            </div>
        </div>
    </div>
@endsection
