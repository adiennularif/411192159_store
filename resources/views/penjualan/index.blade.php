@extends('layout/main')

@section('title', 'Daftar Penjualan')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-10">  
                <h1 class="mt-5">List Penjualan</h1>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">No Penjualan</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Kode Pelanggan</th>
                            <th scope="col">Kode Barang</th>
                            <th scope="col">Jumlah Barang</th>
                            <th scope="col">Harga Barang</th> 
                            <th scope="col">Aksi</th> 
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $penjualan as $pnjln)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $pnjln->no_penjualan }}</td>
                            <td>{{ $pnjln->tanggal }}</td>
                            <td>{{ $pnjln->kode_pelanggan }}</td>
                            <td>{{ $pnjln->kode_barang }}</td>
                            <td>{{ $pnjln->jumlah_barang }}</td>
                            <td>{{ $pnjln->harga_barang }}</td>
                            <td>
                                <a href="" class="badge badge-success">Edit</a>
                                <a href="" class="badge badge-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection
