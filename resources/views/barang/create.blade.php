@extends('layout/main')

@section('title', 'Form Tambah Barang')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-10">  
                <h1 class="mt-3">Form Tambah Barang</h1>
                <form method="POST" action="/barang" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="kode_barang" class="form-label">Kode Barang</label>
                        <input type="number" class="form-control" id="kode_barang" placeholder="Masukan Kode Barang" name="kode_barang">
                    </div>
                    <div class="form-group">
                        <label for="nama_barang" class="form-label">Nama Barang</label>
                        <input type="text" class="form-control" id="nama_barang" placeholder="Masukan Nama Barang" name="nama_barang">
                    </div>
                    <div class="form-group">
                        <label for="deskripsi" class="form-label">Desktipsi</label>
                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Deskripsi" name="deskripsi">
                    </div>
                    <div class="form-group">
                        <label for="stok_barang" class="form-label">Stok Barang</label>
                        <input type="number" class="form-control" id="stok_barang" placeholder="Masukan Stok Barang" name="stok_barang">
                    </div>
                    <div class="form-group">
                        <label for="harga_barang" class="form-label">Harga Barang</label>
                        <input type="number" class="form-control" id="harga_barang" placeholder="Masukan Harga Barang" name="harga_barang">
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah Data!</button>
                </form>
            </div>
        </div>
    </div>
@endsection
