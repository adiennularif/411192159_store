<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//index
Route::get('/', 'PagesController@home');
Route::get('/barang', 'BarangController@index');
Route::get('/pelanggan', 'PelangganController@index');
Route::get('/penjualan', 'PenjualanController@index');

//create
Route::get('/barang/create', 'BarangController@create');
Route::get('/pelanggan/create', 'PelangganController@create');

//edit
Route::get('{barang}/edit', 'BarangController@edit');

//update
Route::patch('/barang/{barang}', 'BarangController@update');
Route::patch('/pelanggan/{pelanggan}', 'PelangganController@update');

//store
Route::post('/barang', 'BarangController@store');
Route::post('/pelanggan', 'PelangganController@store');

//delete
Route::delete('{barang}', 'BarangController@destroy');
Route::delete('{pelanggan}', 'PelangganController@destroy');
